//
//  ExpenseViewController.swift
//  ExpenseTracker
//
//  Created by Cory Dunn on 10/13/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class ExpenseViewController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var amountTxtField: UITextField!
    @IBOutlet var locationTxtField: UITextField!
    @IBOutlet var categoryTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Add Expense"
    }
    
    @IBAction func cancelBtnTouched(_ sender: AnyObject) {
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: nil)
    }
    
    @IBAction func saveBtnTouched(_ sender: AnyObject) {
        let dictionary: [String:AnyObject] = ["date":datePicker.date as AnyObject, "amount":amountTxtField as AnyObject, "location":locationTxtField as AnyObject, "category": categoryTxtField as AnyObject]
        
        
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: dictionary)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
