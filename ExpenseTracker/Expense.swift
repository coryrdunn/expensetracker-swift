//
//  Expense.swift
//  ExpenseTracker
//
//  Created by Cory Dunn on 10/13/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

struct Expense {
    
    var date: Date
    var amount: String?
    var location: String?
    var category: String?
    
    init(date: Date, amount: String?, location: String?, category: String?) {
        self.date = date
        self.amount = amount
        self.location = location
        self.category = category
    }
    
}
