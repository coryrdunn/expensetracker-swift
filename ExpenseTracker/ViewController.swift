//
//  ViewController.swift
//  ExpenseTracker
//
//  Created by Cory Dunn on 10/12/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    @IBOutlet var addBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Expenses"
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dismissAddExpenseVC), name: NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: nil)
    }
    
    func dismissAddExpenseVC(notif: Notification ) -> Void {
        if (notif.object != nil) {
            
            
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "sortByDate"), object: notif.object)
            
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "sortByCategory"), object: notif.object)
            
        }
        
        
        
        self.dismiss(animated: true) {
            
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

