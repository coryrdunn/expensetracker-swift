//
//  ByDateViewController.swift
//  ExpenseTracker
//
//  Created by Cory Dunn on 10/13/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit


class ByDateViewController: UIViewController{

    @IBOutlet var dateTableView: UITableView!
    var ExpenseByDate = [[String:AnyObject]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ByDateViewController.sortByDate), name: NSNotification.Name(rawValue: "sortByDate"), object: nil)
    }
    

    func sortByDate(notif: Notification ) -> Void {
        if (notif.object != nil) {
        
            ExpenseByDate.append(notif.object as! [String : AnyObject])
        
            ExpenseByDate.sort{
                (($0 )["date"] as? Date)! < (($1 )["date"] as? Date)!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ExpenseByDate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath)
        
        let expense = ExpenseByDate[indexPath.row]
        
        cell.textLabel?.text = expense["date"] as! String?
        
        return cell
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
