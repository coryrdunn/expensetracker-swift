//
//  ByCategoryViewController.swift
//  ExpenseTracker
//
//  Created by Cory Dunn on 10/13/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class ByCategoryViewController: UIViewController {

    @IBOutlet var categoryTableView: UITableView!
    var ExpenseByCategory = [[String:AnyObject]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ByCategoryViewController.sortByCategory), name: NSNotification.Name(rawValue: "sortByCategory"), object: nil)
    }
    
    
    func sortByCategory(notif: Notification ) -> Void {
        if (notif.object != nil) {
            
            ExpenseByCategory.append(notif.object as! [String : AnyObject])
            
            ExpenseByCategory.sort{
                (($0 )["category"] as? String)! < (($1 )["category"] as? String)!
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ExpenseByCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath)
        
        let dictionary = ExpenseByCategory[indexPath.row]
        
        cell.textLabel?.text = dictionary["date"] as! String?
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
